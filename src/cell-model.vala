public class Rm.CellModel : Object {
    public unowned BatteryModel battery { get; private set; }
    public uint index { get; private set; }
    public uint voltage { get; set; }
    public float level { get; set; }

    public CellModel (BatteryModel battery, uint index) {
	this.battery = battery;
	this.index = index;

	bind_property ("voltage", this, "level",
		       BindingFlags.SYNC_CREATE,
		       transform_voltage_level);
    }

    static bool transform_voltage_level (Binding binding,
					 Value voltage,
					 ref Value level) {
	var v = voltage.get_uint ();

	message ("transform %u %f", voltage.get_uint (), level.get_float ());
	level.set_float ((float) (v - 3200) / (4200 - 3200));

	return true;
    }
}
