public class Rm.Application : Adw.Application {
    private Settings settings;
    private MainWindow window;

    public Application () {
	Object (application_id: "reactor.manager");

	settings = new Settings (application_id);
	assert (settings != null);
    }

    protected override void activate () {
	base.activate ();

	var unused = new SList<Type> ();

	unused.prepend (typeof (ComView));
	unused.prepend (typeof (ConsoleView));
	unused.prepend (typeof (BatteryView));
	unused.prepend (typeof (ConfigView));
	unused.prepend (typeof (RegistersView));
	unused.prepend (typeof (StructureView));

	window = new MainWindow (this);
	window.show ();
	window.load_settings (settings);
    }
}
