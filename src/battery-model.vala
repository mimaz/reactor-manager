public class Rm.BatteryModel : Object {
    public string name { get; set; default = "unnamed"; }
    public uint num_cells {
	get {
	    return cell_array.length;
	}
	set {
	    while (value > cell_array.length) {
		cell_array.add (new CellModel (this, cell_array.length));
	    }

	    cell_array.remove_range (value, cell_array.length - value);
	}
    }
    public uint voltage { get; set; }
    public int current { get; set; }
    public BCom.BatConf bat_conf { get; set; }
    public BCom.ProtConf prot_conf { get; set; }

    private GenericArray<CellModel> cell_array;

    construct {
	bat_conf = new BCom.BatConf ();
	prot_conf = new BCom.ProtConf ();

	cell_array = new GenericArray<CellModel> ();
    }

    public CellModel get_cell (uint index) {
	return cell_array.get (index);
    }
}
