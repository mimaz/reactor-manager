[GtkTemplate (ui="/rm/curve-row-view.ui")]
public class Rm.CurveRowView : Gtk.ListBoxRow {
    public int32 x_value { get; set; }
    public int32 y_value { get; set; }
    public Gtk.Adjustment y_adjustment { get; set; }

    public string x_label { get; set; }
    public string y_label { get; set; }

    [GtkChild] unowned Gtk.Label x_label_widget;
    [GtkChild] unowned Gtk.Label y_label_widget;
    [GtkChild] unowned Gtk.Scale scale;

    construct {
	y_adjustment = new Gtk.Adjustment (0, 0, 0, 1, 0, 0);

	bind_property ("x-value", this, "x-label",
		       BindingFlags.SYNC_CREATE);
	bind_property ("y-value", this, "y-label",
		       BindingFlags.SYNC_CREATE);
	bind_property ("y-value", y_adjustment, "value",
		       BindingFlags.SYNC_CREATE |
		       BindingFlags.BIDIRECTIONAL);
    }

    public CurveRowView (Gtk.SizeGroup x_label_group,
			 Gtk.SizeGroup y_label_group,
			 Gtk.SizeGroup spin_group) {
	x_label_group.add_widget (x_label_widget);
	y_label_group.add_widget (y_label_widget);
	spin_group.add_widget (scale);
    }
}
